const http = require('http');
const WebSocket = require('ws');
const url = require('url');
const fs = require('fs');
const Handlebars = require('handlebars');
const Database = require('better-sqlite3');
var Gpio = require('onoff').Gpio;
var DOOR_SENSOR = new Gpio(4, 'in');
var index_tmpl = Handlebars.compile(fs.readFileSync('./index.html').toString())
var MIN_TOILET_SESSION = 1000; // in ms
var PORT = 8080

// We setup the database
const db = new Database('toilet-sessions.db');
// check to see if we already initialized this database
let stmt = db.prepare(`SELECT name
	FROM sqlite_master
	WHERE
		type='table' and name='session'
	;`);
let row = stmt.get();
if(row === undefined){
	console.log("WARNING: database appears empty; initializing it.");
	const sql_init_commands = `
		CREATE TABLE session (
			id   INTEGER PRIMARY KEY,
			started_at INTEGER,
			ended_at INTEGER
		);
	`;
	db.exec(sql_init_commands);
}


// We start the server
var index_html = index_tmpl({PORT:PORT});

const server = http.createServer();

const wss = new WebSocket.Server({noServer: true });

let is_occupied = undefined;
let prev_state  = undefined;
let prev_time = Date.now();

server.on('request', (request, response) => {
	let pathname = url.parse(request.url).pathname
	if(pathname == "/"){
		response.statusCode = 200;
		response.setHeader('Content-Type', 'text/html; charset=utf-8');
		response.write(index_html);
		response.end();
	}else if(pathname == "/history"){
		response.statusCode = 200;
		response.setHeader('Content-Type', 'application/json');
		const stmt = db.prepare('SELECT * FROM session');
		const sessions = stmt.all();
		var data_str = JSON.stringify({history: sessions.map((c)=> {
			return {started_at: c.started_at, ended_at: c.ended_at}
		})})
		response.write(data_str);
		response.end();
	}else if(pathname == "/status"){
		response.statusCode = 200;
		response.setHeader('Content-Type', 'application/json');
		var data_str = JSON.stringify({is_occupied: is_occupied, updated_at: prev_time})
		response.write(data_str);
		response.end();
	}else{
		response.statusCode = 404;
		response.end();
	}
});

wss.on('connection', function connection(ws) {
  // ...
});

server.on('upgrade', function upgrade(request, socket, head) {
	const pathname = url.parse(request.url).pathname;
	console.log("user connected")
	wss.handleUpgrade(request, socket, head, function done(ws) {
		wss.emit('connection', ws, request);
	});
});

// We start the server
console.log(`server running on port: ${PORT}`)
server.listen(PORT);

function update_door_state() {
	is_occupied = !DOOR_SENSOR.readSync();
	// is_occupied = Math.random() > 0.5 ? true : false
	var is_first_pass = (typeof prev_state === 'undefined')
	if (is_occupied !== prev_state || is_first_pass){
		console.log(`Door State: ${is_occupied ? 'closed': 'open'}`)
		console.log(`Notifying ${wss.clients.size} users`)
		var time = Date.now()
		// We update the database if the state was maintained for a long enough time
		if(!is_first_pass && !is_occupied && time - prev_time > MIN_TOILET_SESSION){
			const stmt = db.prepare('INSERT INTO session (started_at, ended_at) VALUES (?, ?)');
			stmt.run(prev_time,  time);
		}
		// We notify all the clients
		wss.clients.forEach((client) => {
			if (client.readyState === WebSocket.OPEN) {
				var data_str = JSON.stringify({is_occupied: is_occupied, updated_at: time})
				client.send(data_str);
			}
		});
		prev_state = is_occupied
		prev_time = time
	}
}

// We check the door state every second
update_door_state()
setInterval(update_door_state, 2000)
